// Практичні завдання
//  1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
const a = document.createElement("a");
a.textContent = "Learn More";
a.setAttribute("href", "#");
const footer = document.getElementsByTagName("footer")[0];
footer.append(a);
//  2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
//  Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.
//  */
// const select = document.createElement("select");
// select.setAttribute("id", "rating");
// const features = document.querySelector(".features");
// features.prepend(select);

// const option4 = document.createElement("option");
// option4.setAttribute("value", "4");
// option4.textContent = "4 Stars";
// select.prepend(option4);

// const option3 = document.createElement("option");
// option3.setAttribute("value", "3");
// option3.textContent = "3 Stars";
// select.prepend(option3);

// const option2 = document.createElement("option");
// option2.setAttribute("value", "2");
// option2.textContent = "2 Stars";
// select.prepend(option2);

// const option1 = document.createElement("option");
// option1.setAttribute("value", "1");
// option1.textContent = "1 Star";
// select.prepend(option1);

///////////////////////////////////////////////////////////////
// Example 2
const select = document.createElement("select");
select.setAttribute("id", "rating");
const features = document.querySelector(".features");
features.prepend(select);

const options = [
  { value: "4", text: "4 Stars" },
  { value: "3", text: "3 Stars" },
  { value: "2", text: "2 Stars" },
  { value: "1", text: "1 Star" },
];

for (const optionData of options) {
  const option = document.createElement("option");
  option.setAttribute("value", optionData.value);
  option.textContent = optionData.text;
  select.prepend(option);
}
