// Практичні завдання
// 1. Запитайте у користувача два числа. Перевірте, чи є кожне з введених значень числом. Якщо ні,
//  то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом.
//  Виведіть на екран всі числа від меншого до більшого за допомогою циклу for.

num1 = Number(prompt("input first number"));
while (isNaN(num1) || num1 === 0) {
  num1 = Number(prompt("this is not number, please input a number"));
}
num2 = Number(prompt("input second number"));
while (isNaN(num2) || num2 === 0) {
  num2 = Number(prompt("this is not number, please input a number"));
}

if (num1 > num2) {
  for (let i = num2; i <= num1; i++) {
    console.log(i);
  }
}
if (num2 > num1) {
  for (let i = num1; i <= num2; i++) {
    console.log(i);
  }
}

// 2. Напишіть програму, яка запитує в користувача число та перевіряє,
// чи воно є парним числом. Якщо введене значення не є парним числом,
// то запитуйте число доки користувач не введе правильне значення.
// let num = Number(prompt("input some number"));
// while (isNaN(num) || num === 0 || num % 2 !== 0) {
//   num = Number(prompt("input an even number"));
// }
// console.log(num);
