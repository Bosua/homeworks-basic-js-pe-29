// Практичне завдання:
// Реалізувати можливість зміни колірної теми користувача.

// Технічні вимоги:
// - Взяти готове домашнє завдання HW-4 "Price cards" з блоку Basic HMTL/CSS.

// - Додати на макеті кнопку "Змінити тему".

// - При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд.
// При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.

// - Вибрана тема повинна зберігатися після перезавантаження сторінки.

document.getElementById("checkbox").addEventListener("click", () => {
  if (localStorage.getItem("theme") === "dark") {
    localStorage.removeItem("theme");
  } else {
    localStorage.setItem("theme", "dark");
  }
  addDarkClassToHTML();
});
function addDarkClassToHTML() {
  if (localStorage.getItem("theme") === "dark") {
    document.querySelector("html").classList.add("dark");
  } else {
    document.querySelector("html").classList.remove("dark");
  }
}
addDarkClassToHTML();
