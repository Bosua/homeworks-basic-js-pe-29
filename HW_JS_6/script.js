// Практичні завдання
// 1. Створіть об'єкт product з властивостями name, price та discount.
// Додайте метод для виведення повної ціни товару з урахуванням знижки.
// Викличте цей метод та результат виведіть в консоль.
// product = {
//   name: "",
//   price: 0,
//   discount: 0,
//   checkTotalPrice: function (name, price, discount) {
//     if (discount) {
//       let totalPrice = price - (discount / 100) * price;
//       console.log(
//         `Загальна вартість ${name} із використанням знижки ${discount}% складає: ${totalPrice}$`
//       );
//     } else {
//       console.log(`Загальна вартість ${name} без використанням знижки складає: ${price}$`);
//     }
//   },
// };
// product.checkTotalPrice("Ford Fusion", 14000, 0);
// product.checkTotalPrice("Ford Fusion", 14000, 15);

// 2. Напишіть функцію greeting, яка приймає об'єкт з властивостями name та age,
// і повертає рядок з привітанням і віком,

// наприклад "Привіт, мені 30 років".

// Попросіть користувача ввести своє ім'я та вік

// за допомогою prompt, і викличте функцію gteeting з введеними даними(передавши їх як аргументи).
// Результат виклику функції виведіть з допомогою alert.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// function greeting() {
//   let userInfo = {
//     name: "name",
//     age: 15,
//   };
//   return console.log(`Привіт, мені ${userInfo.age} років`);
// }
// greeting();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function greeting(introduction) {
  let userInfo = {
    name: prompt("Enter your name"),
  };

  while (userInfo.name === "") {
    userInfo.name = prompt("Enter your name correctly");
  }

  userInfo.age = parseInt(prompt("Enter your age"));

  while (isNaN(userInfo.age)) {
    userInfo.age = parseInt(prompt("Enter your age correctly"));
  }
  console.log(userInfo);
  introduction = `Hello, my name is ${userInfo.name} I am  ${userInfo.age} yaers old`;
  return introduction;
}

let result = greeting();
alert(result);

// 3.Опціональне. Завдання:

// Реалізувати повне клонування об'єкта.
// Технічні вимоги:
// - Написати функцію для рекурсивного повного клонування об'єкта (без єдиної передачі за посиланням,
// внутрішня вкладеність властивостей об'єкта може бути досить великою).

// - Функція має успішно копіювати властивості у вигляді об'єктів та масивів на будь-якому рівні вкладеності.

// - У коді не можна використовувати вбудовані механізми клонування, такі як функція Object.assign() або spread.
