/* 
Практичне завдання.
Реалізувати функцію підсвічування клавіш.

Технічні вимоги:

- У файлі index.html лежить розмітка для кнопок.
- Кожна кнопка містить назву клавіші на клавіатурі
- Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. 
При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. 
Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S,
 і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.
Але якщо при натискані на кнопку її  не існує в розмітці, то попередня 
активна кнопка повина стати неактивною.
*/

function backlightKeys() {
  document.addEventListener("keydown", function (event) {
    const keyPressed = event.key.toUpperCase();

    const buttons = document.querySelectorAll(".btn");
    const button = [...buttons].find((btn) => btn.textContent.toUpperCase() === keyPressed);
    const activeBtn = document.querySelector(".active");

    if (button) {
      button.classList.add("active");
      if (activeBtn) {
        activeBtn.classList.remove("active");
      }
    }
    // else {
    //   activeBtn.classList.remove("active");
    // } // в цьому випадку вибиває помилки коли ти одразу нажимаєш не ту кнопку,
    //      у зв'язку з тим, що немає стилю active який потрібно видалити.
    if (button === undefined) {
      console.log("Такої кнопки не існує");
      if (activeBtn) {
        activeBtn.classList.remove("active");
      }
    }
  });
}
backlightKeys();
