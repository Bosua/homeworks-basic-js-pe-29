// 1. Перевірите, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом
// (читається однаково зліва направо і справа наліво), або false в іншому випадку.

// FIRST EXAMPLE
// function isPalindrome(str) {
//   if (
//     str.toLowerCase().split("").reverse().join("").replace(/[\W_]/g, "") ===
//     str.toLowerCase().replace(/[\W_]/g, "")
//   ) {
//     return true;
//   } else {
//     return false;
//   }
// }
// let result = isPalindrome("Три психи пили Пилипихи спирт");
// console.log(result);

// ///////////////////////////////////////////////////////////////////////////////////////
// // SECOND EXAMPLE
// function isPalindrome2(str2) {
//   let reversedStr = "";
//   for (let i = str2.length - 1; i >= 0; i--) {
//     reversedStr = reversedStr + str2[i];
//   }
//   if (
//     str2.toLowerCase().replace(/[\W_]/g, "") === reversedStr.toLowerCase().replace(/[\W_]/g, "")
//   ) {
//     return true;
//   } else {
//     return false;
//   }
// }
// let result2 = isPalindrome2("Три психи пили Пилипихи спирт");
// console.log(result2);
// Це для себе
// метод replace()
// Коли викликається replace(), він шукає всі входження searchValue в originalString і замінює їх на replaceValue
// const newString = originalString.replace(searchValue, replaceValue);

// /[\W_]/ - це шаблон регулярного виразу. Квадратні дужки [] означають набір символів, а \W вказує на будь-який символ,
// який не є буквою (латинською чи цифрою). Символ _ означає, що також буде вилучено нижнє підкреслення.
// /g - прапорець, який означає "глобальний пошук". Він говорить регулярному виразу виконувати пошук і заміну по всьому рядку,
// а не зупинятися після першого збігу.
// '' - це замінювач, який вказує на те, що ми замінюємо знайдені символи на порожній рядок, тобто вилучаємо їх.

// 2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок,
// який потрібно перевірити, максимальну довжину і повертає true,
// якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший.
// Ця функція стане в нагоді для валідації форми. Приклади використання функції:
// let str = "Три психи пили Пилипихи спирт";
// function checkLengthStr(str, maxLength) {
//   if (str.length <= maxLength) {
//     return true;
//   } else {
//     return false;
//   }
// }

// // // Рядок коротше 20 символів
// console.log(checkLengthStr("checked string", 20)); // true
// // // Довжина рядка дорівнює 14 символів
// console.log(checkLengthStr("checked string", 10)); // false

// 3. Створіть функцію, яка визначає скільки повних років користувачу.
// Отримайте дату народження користувача через prompt.
// Функція повина повертати значення повних років на дату виклику функцію.

function checkAge() {
  let birthday = prompt("input your birthday (text in dd.mm.yyyy format)", "19.12.1999");
  let validBirthday = new Date(
    `${birthday.slice(6, 10)}-${birthday.slice(3, 5)}-${birthday.slice(0, 2)}`
  );
  let today = new Date();
  let age = today.getFullYear() - validBirthday.getFullYear();
  let m = today.getMonth() - validBirthday.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < validBirthday.getDate())) {
    age--;
  }
  return age;
}
alert(checkAge());
