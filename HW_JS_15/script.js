// Практичне завдання 1:

// -Створіть HTML-файл із кнопкою та елементом div.

// -При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди.
// Новий текст повинен вказувати, що операція виконана успішно.

//////////////////////////////////////////////////////////////////////////////////////////////
// const div = document.getElementById("wrapper");

// document.getElementById("btn").addEventListener("click", function () {
//   setTimeout(() => {
//     div.textContent = "The operation is done successfully";
//   }, 3000);
// });

// Практичне завдання 2:

// Реалізуйте таймер зворотного відліку, використовуючи setInterval.
// При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div.
// Після досягнення 1 змініть текст на "Зворотній відлік завершено".

const div = document.getElementById("wrapper");
let time = 10;
div.textContent = time;
function startCountdown() {
  const interval = setInterval(() => {
    time--;
    div.textContent = time;
    if (time <= 0) {
      clearInterval(interval);
      div.textContent = "Зворотній відлік завершено";
    }
  }, 1000);
}
const btn = document.getElementById("btn").addEventListener("click", startCountdown);
