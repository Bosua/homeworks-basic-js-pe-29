// Практичні завдання
// 1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.
// let num1 = prompt("input the first number");
// while (num1.trim() === "" || isNaN(num1) || (num1.length > 1 && num1.startsWith("0"))) {
//   num1 = prompt("input the first number correctly");
// }

// let num2 = Number(prompt("input the second number"));
// while (isNaN(num2)) {
//   num2 = Number(prompt("input the second number correctly"));
// }
// while (num2 === 0) {
//   num2 = Number(prompt("you can not divide by zero. Choose the correct number "));
// }

// function division(num1, num2) {
//   return num1 / num2;
// }
// let result = division(num1, num2);
// alert(`here is the result: ${result}`);

// 2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.
// let num1 = parseInt(prompt("input the first number"));
// while (isNaN(num1)) {
//   num1 = parseInt(prompt("input the first number correctly"));
// }

// let num2 = parseInt(prompt("input the second number"));
// while (isNaN(num2)) {
//   num2 = parseInt(prompt("input the second number correctly"));
// }
// let operation = prompt("input the operation you want to perform (+, -, *, /)");
// if (operation !== "+" || operation !== "-" || operation !== "*" || operation !== "/") {
//   alert("Such an operation does not exist");
// }
// let result = function () {
//   if (operation === "+") {
//     return num1 + num2;
//   }
//   if (operation === "-") {
//     return num1 - num2;
//   }
//   if (operation === "*") {
//     return num1 * num2;
//   }
//   if (operation === "/") {
//     while (num2 === 0) {
//       num2 = parseInt(prompt("you can not divide by zero. Choose another number"));
//     }
//     return num1 / num2;
//   }
// };
// alert(result());
// console.log(result());

// Технічні вимоги:
// - Отримати за допомогою модального вікна браузера два числа. Провалідувати отримані значення(перевірити, що отримано числа). Якщо користувач ввів не число, запитувати до тих пір, поки не введе число

// - Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /. Провалідувати отримане значення. Якщо користувач ввів не передбачене значення, вивести alert('Такої операції не існує').

// - Створити функцію, в яку передати два значення та операцію.

// - Вивести у консоль результат виконання функції.

// 3. Опціонально. Завдання:

// Реалізувати функцію підрахунку факторіалу числа.
let num = parseInt(prompt("input the first number"));
while (isNaN(num)) {
  num = parseInt(prompt("input the first number correctly"));
}
function factorial(n) {
  if (n === 0 || n === 1) {
    return 1;
  } else {
    return n * factorial(n - 1);
  }
}

alert(`factorial of ${num} = ${factorial(num)} `);
// Технічні вимоги:

// - Отримати за допомогою модального вікна браузера число, яке введе користувач.

// - За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.

// - Використовувати синтаксис ES6 для роботи зі змінними та функціями.
