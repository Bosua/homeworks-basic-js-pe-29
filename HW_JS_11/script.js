// Практичні завдання
//  1. Додати новий абзац по кліку на кнопку:
//   По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
const btn = document.getElementById("btn-click");
const content = document.getElementById("content");
function addElement() {
  const p = document.createElement("p");
  p.textContent = "New Paragraph";
  content.append(p);
}
btn.addEventListener("click", addElement);
//  2. Додати новий елемент форми із атрибутами:
//  Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
//   По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
const newBtn = document.createElement("button");

newBtn.setAttribute("id", "btn-input-create");
content.append(newBtn);
newBtn.textContent = "And you can click me";
///////////////////////////////////////////////////////////////////
// Стилізація

const newBtnStyles = {
  backgroundColor: "#007bff",
  color: "#fff",
  padding: "10px 20px",
  border: "none",
  cursor: "pointer",
};
Object.assign(newBtn.style, newBtnStyles);

// Додаю ховер

newBtn.addEventListener("mouseover", function () {
  newBtn.style.backgroundColor = "#0056b3";
});
newBtn.addEventListener("mouseout", function () {
  newBtn.style.backgroundColor = "#007bff";
});
// Додаю інпут при кліку

// !!!  // ЧИ МОЖНА У ФУНКЦІЇ СТВОРЮВАТИ ОБ'ЄКТ?????????????????????????????????????
//
function createInputElement() {
  const inputElement = document.createElement("input");

  const inputAttributes = {
    type: "text",
    placeholder: "Введіть ваш текст",
    name: "myInput",
  };

  for (const [key, value] of Object.entries(inputAttributes)) {
    inputElement.setAttribute(key, value);
  }
  inputElement.style.display = "block";
  inputElement.style.margin = "5px auto";
  content.append(inputElement);
}

newBtn.addEventListener("click", createInputElement);
/////////////////////////////////////////////////////////////////////////////////
