// Практичне завдання:

// Реалізувати перемикання вкладок (таби) на чистому Javascript.

// Технічні вимоги:

// - У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки.
// При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
// - Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// - Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися.
// При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

//  Умови:
//  - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
// */

document.addEventListener("DOMContentLoaded", function () {
  const tabs = document.querySelectorAll(".tabs-title");
  const contentWrapper = document.querySelector(".tabs-content");
  const contents = [...contentWrapper.children];
  tabs.forEach((tab, index) => {
    tab.setAttribute("data-content", `content${index + 1}`);

    tab.addEventListener("click", function () {
      const activeTab = document.querySelector(".active");

      if (activeTab) {
        activeTab.classList.remove("active");
      }

      tab.classList.add("active");

      //////////////////////////////////////////////////////////////////
      const target = tab.getAttribute("data-content");

      contents.forEach((content) => {
        if (content.id === target) {
          content.style.display = "block";
        } else {
          content.style.display = "none";
        }
      });
    });
  });
  contents.forEach((content, index) => {
    content.setAttribute("id", `content${index + 1}`);
    content.style.display = "none";
  });
});
