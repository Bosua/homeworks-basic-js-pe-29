// 1. Створіть масив з рядків "travel", "hello", "eat", "ski", "lift"
// та обчисліть кількість рядків з довжиною більше за 3 символи. Вивести це число в консоль.
let array = ["travel", "hello", "eat", "ski", "lift"];

const countLetterByReduce = function (array) {
  return array.reduce(function (count, word) {
    if (word.length > 3) {
      return count + 1;
    } else {
      return count;
    }
  }, 0);
};

console.log(countLetterByReduce(array));
//////////////////////////////////////////////////
const countLetterByfilter = (array) => {
  return array.filter((word) => word.length > 3).length; // ['travel', 'hello', 'lift'] = length = 3
};
console.log(countLetterByfilter(array));
// 2. Створіть масив із 4 об'єктів, які містять інформацію про людей: {name: "Іван", age: 25, sex: "чоловіча"}.
// Наповніть різними даними.
// Відфільтруйте його, щоб отримати тільки об'єкти зі sex "чоловіча".
// Відфільтрований масив виведіть в консоль

let array2 = [
  { name: "Іван", age: 25, sex: "чоловіча" },
  { name: "Юлія", age: 22, sex: "жіноча" },
  { name: "Катерина", age: 18, sex: "жіноча" },
  { name: "Максим", age: 20, sex: "чоловіча" },
];
const filterOfSex = function (array) {
  return array.filter((user) => user.sex === "чоловіча");
};
console.log(filterOfSex(array2));
// 3. Реалізувати функцію фільтру масиву за вказаним типом даних. (Опціональне завдання)
// - Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
// - Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом.
// Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].
let array3 = ["hello", "world", 23, "23", null];
const filterBy = (arr, type) => arr.filter((item) => typeof item !== typeof type);
console.log(filterBy(array3, "string"));
