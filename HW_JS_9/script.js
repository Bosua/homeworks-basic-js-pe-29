// Практичні завдання
//  1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
//  Використайте 2 способи для пошуку елементів.
//  Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
/////////////////////////////////////////////////////////////////////////////
// Перший спосіб отримання елементу:

// const articles = document.getElementsByClassName("feature");

// Array.from(articles).forEach((article) => {
//   article.style.textAlign = "center";
// });
/////////////////////////////////////////////////////////////////////////////
// Другий спосіб отримання елементу:
const articles = document.querySelectorAll(".feature");
console.log(articles);

for (let article of articles) {
  article.style.textAlign = "center";
  console.log(article);
}

// articles.forEach((article) => {
//   article.style.textAlign = "center";
// });
/////////////////////////////////////////////////////////////////////////////
//  2. Змініть текст усіх елементів h2 на "Awesome feature".
// const headings = document.querySelectorAll(".feature-title");
const headings = document.getElementsByTagName("h2");
for (let heading of headings) {
  heading.textContent = "Awesome feature";
}
/////////////////////////////////////////////////////////////////////////////
//  3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
const featureTitles = document.querySelectorAll(".feature-title");
for (let featureTitle of featureTitles) {
  featureTitle.textContent += "!";
}
